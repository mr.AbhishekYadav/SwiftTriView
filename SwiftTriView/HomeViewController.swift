//
//  HomeViewController.swift
//  SwiftTriView
//
//  Created by Abhishek Yadav on 11/06/24.
//

import UIKit

class HomeViewController: UIViewController {
    
    let usernameTextField = UITextField()
    let passwordTextField = UITextField()
    let hintLabel = UILabel()
    let loginButton = UIButton(type: .system)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    private func initialConfig() {
        configureGradientBackground()
        configureViewComponents()
        configureConstraints()
        addTextFieldTargets()
    }
    
    private func configureGradientBackground() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.systemBlue.cgColor, UIColor.systemTeal.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = view.bounds
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    private func configureViewComponents() {
        title = "Home"
        
        usernameTextField.placeholder = "Username"
        usernameTextField.borderStyle = .roundedRect
        usernameTextField.autocapitalizationType = .none
        usernameTextField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(usernameTextField)
        
        passwordTextField.placeholder = "Password"
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.autocapitalizationType = .none
        passwordTextField.isSecureTextEntry = true
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(passwordTextField)
        
        hintLabel.text = "*Password is 123abc"
        hintLabel.font = UIFont.systemFont(ofSize: 14)
        hintLabel.textColor = .black
        hintLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(hintLabel)
        
        loginButton.setTitle("Login", for: .normal)
        loginButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.backgroundColor = .systemBlue
        loginButton.layer.cornerRadius = 10
        loginButton.layer.shadowColor = UIColor.black.cgColor
        loginButton.layer.shadowOpacity = 0.3
        loginButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        loginButton.layer.shadowRadius = 4
        loginButton.addTarget(self, action: #selector(goToDetail), for: .touchUpInside)
        loginButton.isEnabled = false
        loginButton.alpha = 0.5
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(loginButton)
    }
    
    private func configureConstraints() {
        NSLayoutConstraint.activate([
            usernameTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            usernameTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -80),
            usernameTextField.widthAnchor.constraint(equalToConstant: 200),
            usernameTextField.heightAnchor.constraint(equalToConstant: 40),
            
            passwordTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            passwordTextField.topAnchor.constraint(equalTo: usernameTextField.bottomAnchor, constant: 20),
            passwordTextField.widthAnchor.constraint(equalToConstant: 200),
            passwordTextField.heightAnchor.constraint(equalToConstant: 40),
            
            hintLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            hintLabel.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 5),
            
            loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loginButton.topAnchor.constraint(equalTo: hintLabel.bottomAnchor, constant: 20),
            loginButton.widthAnchor.constraint(equalToConstant: 200),
            loginButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func addTextFieldTargets() {
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    @objc private func textFieldDidChange() {
        let isFormFilled = !(usernameTextField.text?.isEmpty ?? true) && !(passwordTextField.text?.isEmpty ?? true)
        loginButton.isEnabled = isFormFilled
        loginButton.alpha = isFormFilled ? 1.0 : 0.5
        
        resetTextFieldBorders()
    }
    
    private func resetTextFieldBorders() {
        usernameTextField.layer.borderColor = UIColor.clear.cgColor
        usernameTextField.layer.borderWidth = 0
        passwordTextField.layer.borderColor = UIColor.clear.cgColor
        passwordTextField.layer.borderWidth = 0
    }
    
    @objc private func goToDetail() {
        guard passwordTextField.text == "123abc" else {
            animateInvalidPassword()
            return
        }
        
        animateLoginButton {
            self.presentDetailViewController()
        }
    }
    
    private func animateInvalidPassword() {
        UIView.animate(withDuration: 0.3, animations: {
            self.passwordTextField.layer.borderColor = UIColor.red.cgColor
            self.passwordTextField.layer.borderWidth = 2
        }) { _ in
            UIView.animate(withDuration: 0.3) {
                self.passwordTextField.layer.borderColor = UIColor.clear.cgColor
                self.passwordTextField.layer.borderWidth = 0
            }
        }
    }
    
    private func animateLoginButton(completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0.2, animations: {
            self.loginButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }) { _ in
            UIView.animate(withDuration: 0.2) {
                self.loginButton.transform = .identity
            } completion: { _ in
                completion()
            }
        }
    }
    
    private func presentDetailViewController() {
        let detailVC = DetailViewController()
        let navigationController = UINavigationController(rootViewController: detailVC)
        navigationController.modalPresentationStyle = .fullScreen
        
        UIView.transition(with: self.view, duration: 0.5, options: .transitionFlipFromRight, animations: {
            self.present(navigationController, animated: false, completion: nil)
        }, completion: nil)
    }
}
