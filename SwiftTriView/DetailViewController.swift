//
//  DetailViewController.swift
//  SwiftTriView
//
//  Created by Abhishek Yadav on 11/06/24.
//

import UIKit

class DetailViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Detail"
        
        // Set gradient background
        setGradientBackground()
        
        // Create and customize "Go to Profile" button
        let profileButton = UIButton(type: .system)
        profileButton.setTitle("Go to Profile", for: .normal)
        profileButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        profileButton.setTitleColor(.white, for: .normal)
        profileButton.backgroundColor = .systemGreen
        profileButton.layer.cornerRadius = 10
        profileButton.layer.shadowColor = UIColor.black.cgColor
        profileButton.layer.shadowOpacity = 0.3
        profileButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        profileButton.layer.shadowRadius = 4
        profileButton.addTarget(self, action: #selector(goToProfile), for: .touchUpInside)
        
        // Create and customize "Logout" button
        let logoutButton = UIButton(type: .system)
        logoutButton.setTitle("Logout", for: .normal)
        logoutButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        logoutButton.setTitleColor(.white, for: .normal)
        logoutButton.backgroundColor = .systemRed
        logoutButton.layer.cornerRadius = 10
        logoutButton.layer.shadowColor = UIColor.black.cgColor
        logoutButton.layer.shadowOpacity = 0.3
        logoutButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        logoutButton.layer.shadowRadius = 4
        logoutButton.addTarget(self, action: #selector(logout), for: .touchUpInside)
        
        // Add and layout buttons
        profileButton.translatesAutoresizingMaskIntoConstraints = false
        logoutButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(profileButton)
        view.addSubview(logoutButton)
        
        NSLayoutConstraint.activate([
            profileButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            profileButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -30),
            profileButton.widthAnchor.constraint(equalToConstant: 200),
            profileButton.heightAnchor.constraint(equalToConstant: 50),
            
            logoutButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoutButton.topAnchor.constraint(equalTo: profileButton.bottomAnchor, constant: 20),
            logoutButton.widthAnchor.constraint(equalToConstant: 200),
            logoutButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    // Gradient background setup
    private func setGradientBackground() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.systemBlue.cgColor, UIColor.systemTeal.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = view.bounds
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    @objc func goToProfile() {
        let profileVC = ProfileViewController()
        navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @objc func logout() {
        dismiss(animated: true, completion: nil)
    }
}
