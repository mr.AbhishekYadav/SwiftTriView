//
//  APIService.swift
//  SwiftTriView
//
//  Created by Abhishek Yadav on 11/06/24.
//

import Foundation

struct APIService {
    static let shared = APIService()
    
    func fetchData(completion: @escaping (Result<String, Error>) -> Void) {
        guard let url = URL(string: "https://api.example.com/data") else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                return
            }
            
            let dataString = String(data: data, encoding: .utf8) ?? "No Data"
            completion(.success(dataString))
        }.resume()
    }
}
