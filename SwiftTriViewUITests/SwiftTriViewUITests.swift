//
//  SwiftTriViewUITests.swift
//  SwiftTriViewUITests
//
//  Created by Abhishek Yadav on 11/06/24.
//

import XCTest

final class SwiftTriViewUITests: XCTestCase {
    
    override func setUpWithError() throws {
        continueAfterFailure = false
        XCUIApplication().launch()
    }
    
    func testNavigation() throws {
        let app = XCUIApplication()
        
        // Test Home to Detail navigation
        app.buttons["Go to Detail"].tap()
        XCTAssertTrue(app.navigationBars["Detail"].exists)
        
        // Test Detail to Profile navigation
        app.buttons["Go to Profile"].tap()
        XCTAssertTrue(app.navigationBars["Profile"].exists)
    }
    
    func testAPIResponse() throws {
        let app = XCUIApplication()
        app.buttons["Go to Detail"].tap()
        app.buttons["Go to Profile"].tap()
        
        let label = app.staticTexts.element(matching: .any, identifier: nil)
        XCTAssertTrue(label.exists)
    }
    
    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
